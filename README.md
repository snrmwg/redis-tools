redis-tools
===========

Some tools written in Go for redis.

These are my first steps with the Go language. Feedback about improvements are appreciated.

Features
-------

* `redis-wait-for-bgsave` performs a BGSAVE and returns when LASTSAVE reports that the save is finished. I use it in some backups scripts. Usage:
  ```
  # show usage

  > redis-wait-for-bgsave -help

  Usage of /root/redis-waitfor-bgsave:
  -addr string
        redis server address (default "localhost")
  -delay string
        delay between LASTSAVE checks (default "1s")
  -help
        show this help message

  # sample call

  > redis-wait-for-bgsave -addr redis-server:6379
  LASTSAVE: 1488107239
  . . . .
  >
  ```
