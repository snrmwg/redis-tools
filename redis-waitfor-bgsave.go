package main

import (
	"flag"
	"fmt"
	"github.com/garyburd/redigo/redis"

	"os"
	"time"
)

func main() {
	helpPtr := flag.Bool("help", false, "show this help message")
	redisAddrPtr := flag.String("addr", "localhost", "redis server address")
	delayBetweenChecksPtr := flag.String("delay", "1s", "delay between LASTSAVE checks")

	flag.Parse()
	if *helpPtr {
		flag.PrintDefaults()
		os.Exit(1)
	}

	delay, err := time.ParseDuration(*delayBetweenChecksPtr)
	if err != nil {
		fmt.Printf("error parsing delay duration: %s\n", err)
		os.Exit(-1)
	}

	conn, err := redis.Dial("tcp", *redisAddrPtr)
	if err != nil {
		fmt.Printf("error connecting to redis: %s\n", err)
		os.Exit(-1)
	}

	lastLastsave, _ := redis.Int(conn.Do("LASTSAVE"))
	fmt.Printf("LASTSAVE: %d\n", lastLastsave)
	conn.Do("BGSAVE")
	for true {
		lastsave, _ := redis.Int(conn.Do("LASTSAVE"))
		fmt.Print(". ")
		if lastsave > lastLastsave {
			break
		}

		time.Sleep(delay)
	}
	fmt.Println()
	defer conn.Close()
}
